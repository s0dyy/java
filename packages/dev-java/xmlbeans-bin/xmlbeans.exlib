# Copyright 2009-2012 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

# I'm too stupid to get xmlbeans to compile from source, I'm afraid, so I shall
# be using this for now.

export_exlib_phases src_install

MY_PN="${PN/-bin}"

SUMMARY="A technology for accessing XML by binding it to Java types"
DESCRIPTION="
XMLBeans is a tool that allows you to access the full power of XML in a Java
friendly way. The idea is that you can take advantage of the richness and features
of XML and XML Schema and have these features mapped as naturally as possible to
the equivalent Java language and typing constructs. XMLBeans uses XML Schema to
compile Java interfaces and classes that you can then use to access and modify
XML instance data. Using XMLBeans is similar to using any other Java interface/class,
you will see things like getFoo or setFoo just as you would expect when working
with Java. While a major use of XMLBeans is to access your XML instance data with
strongly typed Java classes there are also API's that allow you access to the full
XML infoset (XMLBeans keeps XML Infoset fidelity) as well as to allow you to reflect
into the XML schema itself through an XML Schema Object model.
"
HOMEPAGE="http://${MY_PN}.apache.org"
DOWNLOADS="mirror://apache/${MY_PN}/binaries/${MY_PN}-${PV}.tgz"

UPSTREAM_CHANGELOG="http://svn.apache.org/viewvc/${MY_PN}/trunk/CHANGES.txt?revision=666108&view=markup"
UPSTREAM_DOCUMENTATION="http://${MY_PN}.apache.org/docs/${PV}/reference/index.html [[ lang = en ]]"
UPSTREAM_RELEASE_NOTES="http://${MY_PN}.apache.org/news.html"

BUGS_TO="philantrop@exherbo.org"

LICENCES="Apache-2.0"

SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    run:
        virtual/jre
        dev-java/jsr173[>=1.0]
        dev-java/xml-commons-resolver[>=1.2]
"

WORK=${WORKBASE}/${MY_PN}-${PV}
ANT_BUILD_DIR=${WORK}/lib

xmlbeans_src_install() {
    dodir /usr/share/${MY_PN}/lib
    insinto /usr/share/${MY_PN}/lib
    doins "${WORK}"/lib/xbean.jar
    doins "${WORK}"/lib/xbean_xpath.jar
    doins "${WORK}"/lib/xmlpublic.jar

    hereenvd 55${MY_PN} <<EOF
XMLBEANS_HOME=/usr/share/${MY_PN}
EOF
    dodir /usr/share/${MY_PN}/bin
    exeinto /usr/share/${MY_PN}/bin
    doexe "${WORK}"/bin/*
    edo rm "${IMAGE}"/usr/share/${MY_PN}/bin/*.cmd
}

